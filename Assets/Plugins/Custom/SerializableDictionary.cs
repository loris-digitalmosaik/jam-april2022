using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
    [SerializeField]
    private List<KeyValueData> keyValueDatas = new List<KeyValueData>();

    // save the dictionary to lists
    public void OnBeforeSerialize()
    {
        this.keyValueDatas.Clear();
        foreach (KeyValuePair<TKey, TValue> pair in this)
        {
            this.keyValueDatas.Add(new KeyValueData() { key = pair.Key, value = pair.Value });
        }
    }

    // load dictionary from lists
    public void OnAfterDeserialize()
    {
        this.Clear();

        foreach (var kvp in this.keyValueDatas)
        {
            this.Add(kvp.key, kvp.value);
        }
    }

    [Serializable]
    private struct KeyValueData
    {
        public TKey key;
        public TValue value;
    }
}

//esempio
[Serializable] public class DictionaryOfStringAndInt : SerializableDictionary<string, int> { }