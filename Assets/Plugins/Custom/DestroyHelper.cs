using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DestroyHelper
{
    public static void DestroyOnEnd(this AudioSource audioSource)
    {
        audioSource.gameObject.AddComponent<DestroyOnClipEndMoneBehaviour>();
    }
    
    public static void DestroyOnEnd(this ParticleSystem particleSystem)
    {
        particleSystem.gameObject.AddComponent<DestroyOnParticleEndMoneBehaviour>();
    }

    private class DestroyOnClipEndMoneBehaviour : MonoBehaviour
    {
        private IEnumerator Start()
        {
            AudioSource audioSource = gameObject.GetComponent<AudioSource>();
            yield return new WaitWhile(() => audioSource.isPlaying);
            Destroy(gameObject);
        }
    }
    
    private class DestroyOnParticleEndMoneBehaviour : MonoBehaviour
    {
        private IEnumerator Start()
        {
            ParticleSystem ps = gameObject.GetComponent<ParticleSystem>();
            yield return new WaitWhile(() => ps.isPlaying);
            Destroy(gameObject);
        }
    }
    
}
