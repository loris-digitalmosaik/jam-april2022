using System;
using UnityEngine;

/// <summary>
/// Singleton class da usare come padre.
/// ha l'accesso pubblico, rimane attiva attraverso le varie scene
/// va in eccezione piuttosto che uccidere un fratello
/// se non presente si genera un istanza
/// </summary>
public class SingletonI<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<T>();
            if (_instance == null)
            {
                GameObject go = new GameObject("[SharedReference]");
                _instance = go.AddComponent<T>();
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null && _instance != (this as T))
        {
            throw new Exception($"Singleton<{typeof(T)}> ha più di un istanza");
        }
        _instance = this as T;
    }
}

// public class ExampleUsage : SingletonI<ExampleUsage>
// {
//   protected override void Awake()
//   {
//     base.Awake();
//   }
// }