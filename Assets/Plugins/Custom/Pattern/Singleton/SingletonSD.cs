using System;
using UnityEngine;

/// <summary>
/// DontDestroyOnLoad<br/>
/// soppravvive quello nella nuova scena<br/>
/// se manca non si crea
/// </summary>
public class SingletonSD<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T Instance
    {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<T>();
            if (_instance == null)
                throw new NullReferenceException("Singleton<" + typeof(T) + "> istanza non trovata");
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null && _instance != (this as T))
        {
            Destroy(_instance.gameObject);
        }
        _instance = this as T;
        DontDestroyOnLoad(gameObject);
    }
}

// public class ExampleUsage : SingletonSD<ExampleUsage>
// {
//   protected override void Awake()
//   {
//     base.Awake();
//   }
// }