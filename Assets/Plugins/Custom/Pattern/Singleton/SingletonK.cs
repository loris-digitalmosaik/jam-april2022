using System;
using UnityEngine;

/// <summary>
/// Singleton class da usare come padre.
/// uccide il fratello maggiore
/// avvisa quando restituisce null piuttosto che creare un istanza
/// </summary>
public class SingletonK<T> : MonoBehaviour where T : MonoBehaviour
{
  private static T _instance;
  public static T Instance
  {
    get
    {
      if (_instance == null)
        _instance = FindObjectOfType<T>();
      if (_instance == null)
        throw new NullReferenceException("Singleton<" + typeof(T) + "> istanza non trovata");
      return _instance;
    }
  }

  protected virtual void Awake()
  {
    if (_instance != null && _instance != (this as T))
    {
      Destroy(_instance);
    }
    _instance = this as T;
  }
}

// public class ExampleUsage : SingletonK<ExampleUsage>
// {
//   protected override void Awake()
//   {
//     base.Awake();
//   }
// }
