using System;
using UnityEngine;

/// <summary>
/// OnSceneLoad: survive<br/>
/// multi instances: stay alive, kill newers<br/>
/// no instances: raise exception
/// </summary>
public class SingletonA<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T Instance
    {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<T>();
            if (_instance == null)
                throw new NullReferenceException("Singleton<" + typeof(T) + "> istanza non trovata");
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null && _instance != (this as T))
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
    }
}

// public class ExampleUsage : SingletonA<ExampleUsage>
// {
//   protected override void Awake()
//   {
//     base.Awake();
//   }
// }