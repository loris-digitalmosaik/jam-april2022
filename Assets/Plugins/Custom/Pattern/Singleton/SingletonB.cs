using System;
using UnityEngine;

/// <summary>
/// scope: scene<br/>
/// multi instances: raise exception<br/>
/// no instances: raise exception
/// </summary>
public class SingletonB<T> : MonoBehaviour where T : MonoBehaviour
{
  private static T _instance;
  public static T Instance
  {
    get
    {
      if (_instance == null)
        _instance = FindObjectOfType<T>();
      if (_instance == null)
        throw new NullReferenceException("Singleton<" + typeof(T) + "> istanza non trovata");
      return _instance;
    }
  }

  protected virtual void Awake()
  {
    if (_instance != null && _instance != (this as T))
    {
      throw new Exception($"Singleton<{typeof(T)}> ha più di un istanza");
    }
    _instance = this as T;
  }
}

// public class ExampleUsage : SingletonB<ExampleUsage>
// {
//   protected override void Awake()
//   {
//     base.Awake();
//   }
// }
