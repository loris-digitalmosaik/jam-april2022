using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class PhotoCreator : MonoBehaviour
{
    public readonly string PATH = "/Resources/IslesShots/Isle_";
    public string PhotoName = "";
           
    [Button("Scatta la Foto",ButtonSizes.Gigantic)]
    private void CaptureScreenWithPP()
    {
        Texture2D image = new Texture2D(Camera.main.pixelWidth, Camera.main.pixelHeight, TextureFormat.RGB24, false);
        StartCoroutine(UpdateScreenshotTexture(image));
    }

    public IEnumerator UpdateScreenshotTexture(Texture2D Image)
    {        
        yield return new WaitForEndOfFrame();
        RenderTexture transformedRenderTexture = null;
        RenderTexture renderTexture = RenderTexture.GetTemporary(
            Camera.main.pixelWidth,
            Camera.main.pixelHeight,
            24,
            RenderTextureFormat.ARGB32,
            RenderTextureReadWrite.Default,
            1);
        try
        {
            ScreenCapture.CaptureScreenshotIntoRenderTexture(renderTexture);
            transformedRenderTexture = RenderTexture.GetTemporary(
                Camera.main.pixelWidth,
                Camera.main.pixelHeight,
                24,
                RenderTextureFormat.ARGB32,
                RenderTextureReadWrite.Default,
                1);
            Graphics.Blit(
                renderTexture,
                transformedRenderTexture,
                new Vector2(1.0f, -1.0f),
                new Vector2(0.0f, 1.0f));
            RenderTexture.active = transformedRenderTexture;
            Image.ReadPixels(
                new Rect(0, 0, Camera.main.pixelWidth, Camera.main.pixelHeight),
                0, 0);
        }
        catch (Exception e)
        {
            Debug.Log("Exception: " + e);
            yield break;
        }
        finally
        {
            RenderTexture.active = null;
            RenderTexture.ReleaseTemporary(renderTexture);
            if (transformedRenderTexture != null)
            {
                RenderTexture.ReleaseTemporary(transformedRenderTexture);
            }
        }
        var Bytes = Image.EncodeToJPG();
        Image.Apply();
        DestroyImmediate(Image);
        File.WriteAllBytes(Application.dataPath + PATH + PhotoName + ".jpg", Bytes);
       
    }
}
