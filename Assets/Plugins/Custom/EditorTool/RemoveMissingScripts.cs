#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

public class RemoveMissingScripts : EditorWindow {
    [MenuItem("GameObject/Tools/RemoveMissingScripts", false, -100)]
    private static void ShowWindow() {
        Debug.Log("Start repairing object...");
        foreach (GameObject item in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            nextLayer(item, 0);
        }
        Debug.Log("End repairing object");
    }

    private static void nextLayer(GameObject go, int layer){
        //Debug.Log("layer " + layer + ". Object: " + go.name);
        int counter = GameObjectUtility.RemoveMonoBehavioursWithMissingScript(go);
        Debug.Log(go.name + " at Layer " + layer + " Missing Script found!");
        foreach (Transform child in go.transform){
            nextLayer(child.gameObject, layer + 1);
        }
    }    
}
#endif