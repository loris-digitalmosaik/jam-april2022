using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TimerManager : SingletonB<TimerManager>
{
    public event Action TimeEndEvent;
    
    public GameObject panel;
    public Image fill;
    public float fadeDuration = 0.2f;

    private Coroutine _coroutine;

    protected override void Awake()
    {
        base.Awake();
        panel.SetActive(false);
        panel.transform.localScale = Vector3.zero;
        fill.fillAmount = 1f;
        _coroutine = null;
    }

    public void StartTimer(float time)
    {
        ClearCoroutine();
        _coroutine = StartCoroutine(TimerCO(time));
    }

    public void StopTimer()
    {
        ClearCoroutine();
        panel.SetActive(false);
        fill.fillAmount = 1f;
        panel.transform.localScale = Vector3.zero;
    }

    private void ClearCoroutine()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            panel.transform.DOKill();
            fill.DOKill();
        }
    }

    private IEnumerator TimerCO(float time)
    {
        panel.SetActive(true);
        panel.transform.DOScale(Vector3.one, fadeDuration);
        yield return new WaitForSeconds(fadeDuration);
        fill.DOFillAmount(0f, time);
        yield return new WaitForSeconds(time);
        TimeEndEvent?.Invoke();
        panel.transform.DOScale(Vector3.zero, fadeDuration);
        yield return new WaitForSeconds(fadeDuration);
        panel.SetActive(false);
        fill.fillAmount = 1f;
        panel.transform.localScale = Vector3.zero;
        _coroutine = null;
    }
}
