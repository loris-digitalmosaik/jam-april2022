using System;
using _Scripts.Interaction;
using Sirenix.OdinInspector;
using UnityEngine;

public class SpellReceiver : Receiver
{
    public SpellSO spell;

    [BoxGroup("Audio")]
    public AudioClip receiverClip;
    [BoxGroup("Audio")]
    [Range(0f, 1f)]
    public float receiverVolume;


    private AudioComponent _audioComponent;

    protected virtual void Awake()
    {
        _audioComponent = gameObject.GetComponent<AudioComponent>();
    }

    public override void ActionStart(Invoker invoker = null)
    {
        if (invoker is Speller speller)
        {
            _audioComponent._SetVolume(receiverVolume);
            _audioComponent._PlayClip(receiverClip);
            speller.SetSpell(spell);
            if (triggerOnlyOnce)
            {
                Destroy(gameObject);
            }
        }
    }
    public override void ActionEnd(Invoker invoker = null) { }
}