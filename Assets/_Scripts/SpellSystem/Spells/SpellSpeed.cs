using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellSpeed : Spell
{
    public float respawnDistance = 100f;
    public float delta = 10f;
    public override void Init(Speller spwaner, bool mode)
    {
        base.Init(spwaner, mode);
        transform.SetParent(GameManager.Instance.environment.transform);
        transform.position += Vector3.forward * respawnDistance;
        GameManager.Instance.ChangeSpeed(mode ? delta : -delta);
    }
}