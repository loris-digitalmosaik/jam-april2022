using System;
using System.Collections;
using System.Collections.Generic;
using _Scripts.Interaction;
using UnityEngine;

public class Spell : Receiver
{
    public static event Action<bool> FireEvent;
    public Speller owner;


    public virtual void Init(Speller spwaner, bool modality)
    {
        owner = spwaner;
        FireEvent?.Invoke(modality);
    }
    public override void ActionStart(Invoker invoker = null)
    {
    }
    public override void ActionEnd(Invoker invoker = null)
    {
    }
}
