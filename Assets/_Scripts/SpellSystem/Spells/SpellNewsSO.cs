using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName="SpellNewsSO", menuName = "ScriptableObjects/SpellNewsSO")]
public class SpellNewsSO : SerializedScriptableObject
{
    public string title;
    public string description;
    public bool isReal;
}
