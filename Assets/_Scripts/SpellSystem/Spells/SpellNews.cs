using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class SpellNews : Spell
{
    public SpellSO selfData;
    public InputActionReference inputFire;
    public InputActionReference inputFire2;
    public float respawnDistance = 100f;
    public SpellNewsSO[] data;
    public float chooseTime = 2f;

    private int _index;

    private void Awake()
    {
        
        inputFire.action.Enable();
        inputFire2.action.Enable();
    }

    private void OnEnable()
    {
        inputFire.action.performed += OnInputFire;
        inputFire2.action.performed += OnInputFire2;
    }


    private void OnDisable()
    {
        inputFire.action.performed -= OnInputFire;
        inputFire2.action.performed -= OnInputFire2;
    }
    

    private void OnInputFire(InputAction.CallbackContext context)
    {
        RPCFireSpell(true);
    }
    private void OnInputFire2(InputAction.CallbackContext context)
    {
        RPCFireSpell(false);
    }

    private void OnDestroy()
    {
        TimerManager.Instance.TimeEndEvent -= OnTimeEndEvent;
    }
    private void OnTimeEndEvent()
    {
        RPCFireSpell(Random.Range(0,2) == 0);
    }

    public override void Init(Speller spwaner, bool modality)
    {
        base.Init(spwaner, modality);
        
        _index = Random.Range(0, data.Length);
        Debug.Log($"Start {_index}");
        TimerManager.Instance.StartTimer(chooseTime);
        TimerManager.Instance.TimeEndEvent += OnTimeEndEvent;
        StartCoroutine(AfterInitCO());
    }

    private IEnumerator AfterInitCO()
    {
        yield return new WaitForEndOfFrame();
        SpellView.Instance.OnChangeSpellEvent(selfData);
        yield return new WaitForEndOfFrame();
        SpellView.Instance.OnChangeSpellEvent(selfData);
        SpellView.Instance.SetNews(data[_index]);
    }

    public void RPCFireSpell(bool value)
    {
        transform.SetParent(GameManager.Instance.environment.transform);
        transform.position += Vector3.forward * respawnDistance;
        TimerManager.Instance.StopTimer();
        if (data[_index].isReal == value)
        {
            GameManager.Instance.Jump();
        }
        else
        {
            FailChoice();
        }
        GameManager.Instance.mover.GetComponent<Speller>().SetSpell(null);
        Destroy(gameObject);
    }

    private void FailChoice()
    {
        HealthManager.Instance.LoseHealth();
    }
}
