using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellImmunity : Spell
{
    public float goodSpeed = 120;
    public float badSpeed = 60;
    public override void Init(Speller spwaner, bool modality)
    {
        base.Init(spwaner, modality);
        if (modality)
        {
            StartCoroutine(ChangeSpeedCO(goodSpeed));
        }
        else
        {
            StartCoroutine(ChangeSpeedCO(badSpeed));
        }
    }

    private IEnumerator ChangeSpeedCO(float speed)
    {
        float value = GameManager.Instance.speed;
        GameManager.Instance.speed = speed;
        owner.collider.enabled = false;
        GameManager.Instance.playerAnimator.SetBool("Fast",true);
        yield return new WaitForSeconds(1f);
        GameManager.Instance.playerAnimator.SetBool("Fast",false);
        GameManager.Instance.speed = value;
        owner.collider.enabled = true;
        Destroy(gameObject);
    }
}
