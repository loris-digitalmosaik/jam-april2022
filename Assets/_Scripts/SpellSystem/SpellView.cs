using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpellView : SingletonB<SpellView>
{
    public Image previewImage;
    public TMP_Text nameLabel;

    public Image leftImage;
    public Image rightImage;

    public Image panelNews;
    public TMP_Text titleNews;
    public TMP_Text descriptionNews;
    
    private Speller _speller;
    private string _emptyName;
    private Sprite _emptySprite;

    protected override void Awake()
    {
        base.Awake();
        _emptyName = nameLabel.text;
        _emptySprite = previewImage.sprite;
        nameLabel.gameObject.SetActive(false);
        leftImage.enabled = false;
        rightImage.enabled = false;
    }

    public void Init(Speller speller)
    {
        _speller = speller;
        _speller.ChangeSpellEvent += OnChangeSpellEvent;
        Spell.FireEvent += SpellOnFireEvent;
    }

    public void OnDestroy()
    {
        _speller.ChangeSpellEvent -= OnChangeSpellEvent;
        Spell.FireEvent -= SpellOnFireEvent;
    }
    public void OnChangeSpellEvent(SpellSO spell)
    {
        if (spell != null)
        {
            previewImage.sprite = spell.preview;
            nameLabel.gameObject.SetActive(true);
            nameLabel.text = spell.title;
        }
        else
        {
            previewImage.sprite = _emptySprite;
            nameLabel.gameObject.SetActive(false);
            nameLabel.text = _emptyName;
            
            //if news
            panelNews.gameObject.SetActive(false);
        }
    }

    public void SetNews(SpellNewsSO data)
    {
        panelNews.gameObject.SetActive(true);
        titleNews.text = data.title;
        descriptionNews.text = data.description;
    }

    private void SpellOnFireEvent(bool value)
    {
        StartCoroutine(SelectAnimationCO(value ? rightImage : leftImage));
    }

    private IEnumerator SelectAnimationCO(Image image)
    {
        image.enabled = true;
        yield return new WaitForSeconds(0.1f);
        image.enabled = false;
    }
}
