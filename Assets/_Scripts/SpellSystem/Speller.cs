using System;
using _Scripts.Interaction;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

public class Speller : PlayerInvoker
{
    public event Action<SpellSO> ChangeSpellEvent;

    public InputActionReference inputFire;
    public InputActionReference inputFire2;

    [HideInEditorMode]
    public new Collider collider;

    private int _index;

    private void Awake()
    {
        collider = gameObject.GetComponent<Collider>();
        inputFire.action.Enable();
        inputFire2.action.Enable();
    }

    private void Start()
    {
        SpellView.Instance.Init(this);
        SetSpell(null);
    }

    private void OnEnable()
    {
        inputFire.action.performed += OnInputFire;
        inputFire2.action.performed += OnInputFire2;
    }

    private void OnDisable()
    {
        inputFire.action.performed -= OnInputFire;
        inputFire2.action.performed -= OnInputFire2;
    }

    private void OnInputFire(InputAction.CallbackContext context)
    {
        RPCFireSpell(_index, true);
    }
    private void OnInputFire2(InputAction.CallbackContext context)
    {
        RPCFireSpell(_index, false);
    }

    public void SetSpell(SpellSO spell)
    {
        ChangeSpellEvent?.Invoke(spell);
        _index = -1;
        if (spell == null) return;
        for (int i = 0; i < SpellManager.Instance.permittedSpells.Length; i++)
        {
            if (SpellManager.Instance.permittedSpells[i] == spell)
            {
                _index = i;
                break;
            }
        }
        if (_index == -1)
        {
            Debug.Log($"Spell non permessa: {spell}");
        }
    }

    public void RPCFireSpell(int spellIndex, bool mode = false)
    {
        if (spellIndex == -1) return;
        _index = spellIndex;
        Spell instantiatedSpell = Instantiate(SpellManager.Instance.permittedSpells[spellIndex].prefab, transform.parent).GetComponent<Spell>();
        instantiatedSpell.Init(this, mode);
        SetSpell(null);
    }

    public void OnInputFire()
    {
        RPCFireSpell(_index, true);
    }
}