using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Interaction;

public class SpellReceiverNews : SpellReceiver
{
    private SpellNews _news;
    
    protected override void Awake()
    {
        base.Awake();
    }

    public override void ActionStart(Invoker invoker = null)
    {
        base.ActionStart(invoker);
        if (invoker is Speller speller)
        {
            speller.OnInputFire();
        }
    }
}
