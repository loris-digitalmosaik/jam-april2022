using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "SpellSO", menuName = "ScriptableObjects/Spell")]
public class SpellSO : SerializedScriptableObject
{
    public string title;
    public Sprite preview;
    public GameObject prefab;
}
