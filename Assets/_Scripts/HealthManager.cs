using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : SingletonB<HealthManager>
{
    public Image fillImage;
    public float duration = 1f;
    public float loseAmount = 0.2f;

    [ShowInInspector][HideInEditorMode]
    private float _amount;

    protected override void Awake()
    {
        base.Awake();
        _amount = 1f;
    }

    public void LoseHealth()
    {
        _amount = Mathf.Clamp(_amount - loseAmount, 0f, 1f);
        fillImage.DOFillAmount(_amount, duration).SetEase(Ease.OutElastic);
        StartCoroutine(GameManager.Instance.FallCO());
        if (_amount <= 0f)
        {
            StartCoroutine(GameManager.Instance.EndGame());
        }
    }
}
