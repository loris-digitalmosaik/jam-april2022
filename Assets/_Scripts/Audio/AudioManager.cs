using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Scripts.Audio
{
    public class AudioManager : SingletonA<AudioManager>
    {
        public static event Action<AudioSettings> SetAudioEvent; 

        public AudioTypeSourceDictionary audioSources;
        public AudioSettings Value { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            if (audioSources == null)
            {
                audioSources = new AudioTypeSourceDictionary();
            }
            foreach (object enumValue in Enum.GetValues(typeof(AudioType)))
            {
                AudioType audioType = (AudioType)enumValue;
                if (!audioSources.ContainsKey(audioType))
                {
                    audioSources.Add(audioType, gameObject.AddComponent<AudioSource>());
                }
            }
            audioSources[AudioType.Background].loop = true;
            LoadSettings();
        }

        private void LoadSettings()
        {
            Value = new AudioSettings()
            {
                main = 1f,
                volumes = new AudioTypeFloatDictionary()
                {
                    {AudioType.Background, 0.3f},
                    {AudioType.Effects, 0.3f},
                    {AudioType.UI, 0.5f},
                }
            };
            ApplyChange(Value);
        }

        public void ApplyChange(AudioSettings nextValue)
        {
            // tutti gli audiosource in scena devono ascoltare questo evento
            // usando solamente AudioComponent sono sicuro che i settings siano rispettati
            SetAudioEvent?.Invoke(Value);
            foreach ((AudioType audioType, AudioSource audioSource) in audioSources)
            {
                float volumeMul = 1f;
                if (Value.main > 0f && Value.volumes[audioType] > 0f)
                {
                    volumeMul = audioSource.volume / (Value.main * Value.volumes[audioType]);
                }
                audioSource.volume = volumeMul * nextValue.main * nextValue.volumes[audioType];
            }
            Value = nextValue;
        }
    }
}

[Serializable]
public class AudioSettings
{
    public float main;
    public AudioTypeFloatDictionary volumes;
}

[Serializable]
public enum AudioType
{
    Background,
    Effects,
    UI
}

[Serializable]
public class AudioTypeFloatDictionary : SerializableDictionary<AudioType, float> { }


[Serializable]
public class AudioTypeSourceDictionary : SerializableDictionary<AudioType, AudioSource> { }