using _Scripts.Audio;
using Sirenix.OdinInspector;
using UnityEngine;

public class AudioComponent : MonoBehaviour
{
    [SerializeField]
    private AudioType type;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private bool playClipOnStart;
    [ShowIf("@this.playClipOnStart")]
    [SerializeField]
    private AudioClip clip;
    [ShowIf("@this.playClipOnStart")][Range(0f, 1f)]
    [SerializeField]
    private float volume = 1f;

    private void Start()
    {
        if (playClipOnStart)
        {
            _SetVolume(volume);
            _PlayClip(clip);
        }
    }

    private void OnEnable()
    {
        AudioManager.SetAudioEvent += OnSetAudioEvent;
    }

    private void OnDisable()
    {
        AudioManager.SetAudioEvent -= OnSetAudioEvent;
    }
    private void OnSetAudioEvent(AudioSettings settings)
    {
        // setto solo il proprio audiosource, perchè quello globale è compito del manager
        if (audioSource != null)
        {
            audioSource.volume = settings.main * settings.volumes[type];
        }
    }

    public void _PlayClip(AudioClip audioClip)
    {
        AudioSource currentAudioSource = audioSource == null ? AudioManager.Instance.audioSources[type] : audioSource;
        // se è un audio di background evito di farlo ripartire se già in esecuzione
        if (type == AudioType.Background && currentAudioSource.clip == audioClip) return;
        currentAudioSource.clip = audioClip;
        _Play();
    }

    public void _Play()
    {
        AudioSource currentAudioSource = audioSource == null ? AudioManager.Instance.audioSources[type] : audioSource;
        // cambio il volume perchè potrebbe essere stato cambiato mentre questa componente era spenta o non instanziata
        OnSetAudioEvent(AudioManager.Instance.Value);
        currentAudioSource.Play();
    }

    public void _SetVolume(float partialVolume)
    {
        AudioSource currentAudioSource = audioSource == null ? AudioManager.Instance.audioSources[type] : audioSource;
        currentAudioSource.volume = partialVolume * AudioManager.Instance.Value.main * AudioManager.Instance.Value.volumes[type];
    }

    public void _Stop()
    {
        AudioSource currentAudioSource = audioSource == null ? AudioManager.Instance.audioSources[type] : audioSource;
        currentAudioSource.Stop();
    }
}