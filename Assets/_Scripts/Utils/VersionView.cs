using TMPro;
using UnityEngine;

public class VersionView : MonoBehaviour
{
    public TMP_Text label;

    private void Awake()
    {
        Debug.Log($"v_{Application.version}");
        if (label != null)
        {
            label.text = $"{label.text} v_{Application.version}";
        }
    }
}
