using System;
using UnityEngine;

public class SharedReference : SingletonI<SharedReference>
{
    public Transform cameraTransform;
    protected override void Awake()
    {
        base.Awake();
        Camera mCamera = Camera.main;
        if (mCamera == null)
        {
            throw new Exception("Main Camera non trovata");
        }
        cameraTransform = mCamera.transform;
    }
}
