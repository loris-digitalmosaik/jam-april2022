using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Camera))]
public class ScreenFader : SingletonB<ScreenFader>
{
    [Tooltip("Should the screen fade in when a new level is loaded")]
    public bool fadeOnSceneLoaded = true;
    [ShowIf("@this.fadeOnSceneLoaded")]
    public bool rotatePlayerToOriginalRotation = false;
    [ShowIf("@this.fadeOnSceneLoaded")]
    public Transform playerTransform;

    [Tooltip("Color of the fade. Alpha will be modified when fading in / out")]
    public Color fadeColor = Color.black;

    public float fadeInTime = 1f;
    public float fadeOutTime = 1f;
    
    private GameObject _fadeObject;
    private CanvasGroup _canvasGroup;
    private Tweener _activeTweener;

    protected override void Awake()
    {
        base.Awake();
        
        _fadeObject = new GameObject(nameof(ScreenFader))
        {
            transform =
            {
                parent = gameObject.transform,
                localPosition = new Vector3(0, 0, 0.3f),
                localEulerAngles = Vector3.zero,
            }
        };
        
        Canvas fadeCanvas = _fadeObject.AddComponent<Canvas>();
        fadeCanvas.renderMode = RenderMode.WorldSpace;
        fadeCanvas.sortingOrder = 100; // Make sure the canvas renders on top

        _canvasGroup = _fadeObject.AddComponent<CanvasGroup>();
        _canvasGroup.interactable = false;

        Image fadeImage = _fadeObject.AddComponent<Image>();
        fadeImage.color = fadeColor;
        fadeImage.raycastTarget = false;

        // Stretch the image
        RectTransform fadeObjectRect = _fadeObject.GetComponent<RectTransform>();
        fadeObjectRect.anchorMin = new Vector2(1, 0);
        fadeObjectRect.anchorMax = new Vector2(0, 1);
        fadeObjectRect.pivot = new Vector2(0.5f, 0.5f);
        fadeObjectRect.sizeDelta = new Vector2(1f, 1f);
        fadeObjectRect.localScale = new Vector2(2f, 2f);
        _fadeObject.SetActive(false);
    }


    private void OnEnable()
    {
        if (fadeOnSceneLoaded)
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            _canvasGroup.alpha = 1f;
            // la chiamo qui perchè l'evento SceneManager.sceneLoaded è già successo prima di sottoscrivermi
            OnSceneLoaded();
        }
        else
        {
            _canvasGroup.alpha = 0f;
        }
    }

    private void OnDisable()
    {
        if (fadeOnSceneLoaded)
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
    }

    public void Fade(float fadeIn = Mathf.Infinity, float fadeOut = Mathf.Infinity)
    {
        float timeIn = float.IsPositiveInfinity(fadeIn) ? fadeInTime : fadeIn;
        float timeOut = float.IsPositiveInfinity(fadeOut) ? fadeOutTime : fadeOut;
        if (_activeTweener != null)
        {
            _activeTweener.Kill();
            _activeTweener = null;
        }
        _fadeObject.SetActive(true);
        _activeTweener = DOTween.To(
            () => _canvasGroup.alpha,
            x => _canvasGroup.alpha = x,
            1f,
            timeIn * Time.timeScale).OnComplete(() =>
        {
            _activeTweener = DOTween.To(
                () => _canvasGroup.alpha,
                x => _canvasGroup.alpha = x,
                0f,
                timeOut * Time.timeScale).OnComplete(() =>
            {
                _fadeObject.SetActive(false);
            });
        });
    }

    public void FadeIn(float fadeTime = Mathf.Infinity)
    {
        float time = float.IsPositiveInfinity(fadeTime) ? fadeInTime : fadeTime;
        if (_activeTweener != null)
        {
            _activeTweener.Kill();
            _activeTweener = null;
        }
        _fadeObject.SetActive(true);
        _activeTweener = DOTween.To(
            () => _canvasGroup.alpha,
            x => _canvasGroup.alpha = x,
            1f,
            time * Time.timeScale).SetEase(Ease.OutExpo);
    }
    
    public void FadeOut(float fadeTime = Mathf.Infinity)
    {
        float time = float.IsPositiveInfinity(fadeTime) ? fadeOutTime : fadeTime;
        if (_activeTweener != null)
        {
            _activeTweener.Kill();
            _activeTweener = null;
        }
        _activeTweener = DOTween.To(
            () => _canvasGroup.alpha,
            x => _canvasGroup.alpha = x,
            0f,
            time * Time.timeScale).OnComplete(() =>
        {
            _fadeObject.SetActive(false);
        }).SetEase(Ease.InExpo);
    }


    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        OnSceneLoaded();
    }
    
    private void OnSceneLoaded()
    {
        Fade();
        if (rotatePlayerToOriginalRotation)
        {
            StartCoroutine(RotatePlayer());
        }
    }

    private IEnumerator RotatePlayer()
    {
        float timer = 0f;
        while (timer < fadeInTime + fadeOutTime)
        {
            playerTransform.rotation = Quaternion.identity;
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}