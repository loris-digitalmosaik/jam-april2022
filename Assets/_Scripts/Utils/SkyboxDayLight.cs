using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

public class SkyboxDayLight : MonoBehaviour
{
    private Tweener _activeTweener;
    private Material _skyboxMaterial;
    private int _cubemapTransitionId;
    private bool _phase;
    private float _value;
    
    private void Start()
    {
        _value = DateTime.Now.Minute / 30f;
        _phase = _value >= 1f;
        if (_phase)
        {
            _value -= 1f;
        }
        _skyboxMaterial = RenderSettings.skybox;
        _cubemapTransitionId = Shader.PropertyToID("_CubemapTransition");
        _skyboxMaterial.SetFloat(_cubemapTransitionId, _value);
        StartCoroutine(DayLightCO());
    }

    private IEnumerator DayLightCO()
    {
        while (enabled)
        {
            Debug.Log($"DayLightCO {_value}");
            _value += _phase ? -1 / 30f : +1 / 30f;
            _skyboxMaterial.SetFloat(_cubemapTransitionId, _value);
            if (_value is >= 1f or <= 0f)
            {
                _phase = !_phase;
            }
            yield return new WaitForSeconds(60f);
        }
    }

    // non so perchè ma il materiale cambia
    private void OnDestroy()
    {
        _skyboxMaterial.SetFloat(_cubemapTransitionId, 0f);
    }
}
