using TMPro;
using UnityEngine;

[RequireComponent(typeof(TMP_Text))]
public class FrameRateView : MonoBehaviour
{
    private TMP_Text _label;

    private string _text;
    private int _frameCount = 0;
    private float _dt = 0f;
    private int _fps = 0;
    private const float UPDATE_RATE = 4f; // 4 updates per sec.

    private void Awake()
    {
        _label = gameObject.GetComponent<TMP_Text>();
        _text = _label.text;
    }

    private void Update()
    {
        _frameCount++;
        _dt += Time.deltaTime;
        if (_dt > 1.0/UPDATE_RATE)
        {
            _fps = Mathf.RoundToInt(_frameCount / _dt);
            _frameCount = 0;
            _dt -= 1f/UPDATE_RATE;
        }
        _label.text = $"{_fps}{_text}";
    }
}
