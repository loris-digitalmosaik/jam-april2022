using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class ToggleActiveOnInput : MonoBehaviour
{
    public InputActionReference input;
    public GameObject targetGameObject;

    private void Awake()
    {
        input.action.Enable();
    }

    private void OnEnable()
    {
        input.action.performed += ToggleActive;
    }

    private void OnDisable()
    {
        input.action.performed -= ToggleActive;
    }

    private void ToggleActive(InputAction.CallbackContext context)
    {
        if (targetGameObject != null)
        {
            targetGameObject.SetActive(!targetGameObject.activeSelf);
        }
    }
}