using UnityEngine;
using UnityEngine.InputSystem;

public class InputDebug : MonoBehaviour
{
    public InputActionReference inputAction;

    private void OnEnable()
    {
        if (inputAction == null) return;
        Debug.Log("Premi " + inputAction.name);
        inputAction.action.Enable();
        inputAction.action.performed += _ActionOnperformed;
    }

    private void OnDisable()
    {
        if (inputAction == null) return;
        inputAction.action.performed -= _ActionOnperformed;
    }

    private void _ActionOnperformed(InputAction.CallbackContext context)
    {
        Debug.Log(context);
    }
}