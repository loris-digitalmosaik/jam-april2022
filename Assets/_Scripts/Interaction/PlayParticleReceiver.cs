using _Scripts.Interaction;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayParticleReceiver : Receiver
{
    [InfoBox("Utilizza AudioComponent e ParticleSystem in target")]
    public GameObject target;
    public AudioClip audioClip;
    [Range(0f, 1f)]
    public float audioVolume = 1f;

    private AudioComponent _audioComponent;
    private ParticleSystem _particleSystem;

    private void Start()
    {
        _audioComponent = target.GetComponent<AudioComponent>();
        _particleSystem = target.GetComponent<ParticleSystem>();
        if (_audioComponent == null)
        {
            Debug.LogWarning($"missing AudioComponent in {target}");
        }
        if (_particleSystem == null)
        {
            Debug.LogWarning($"missing ParticleSystem in {target}");
        }
    }

    public override void ActionStart(Invoker invoker = null)
    {
        if (invoker is PlayerInvoker playerInvoker)
        {
            if (_audioComponent != null)
            {
                _audioComponent._SetVolume(audioVolume);
                _audioComponent._PlayClip(audioClip);
            }
            if (_particleSystem != null)
            {
                _particleSystem.Play();
            }
        }
    }
    public override void ActionEnd(Invoker invoker = null) { }
}