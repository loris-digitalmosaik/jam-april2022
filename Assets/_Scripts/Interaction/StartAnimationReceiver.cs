using UnityEngine.Playables;
namespace _Scripts.Interaction
{
    public class StartAnimationReceiver : Receiver
    {
        public PlayableDirector playableDirector;

        public override void ActionStart(Invoker invoker = null)
        {
            if (invoker is PlayerInvoker playerInvoker)
            {
                playableDirector.Play();
                if (triggerOnlyOnce)
                {
                    Destroy(this);
                }
            }
        }
        public override void ActionEnd(Invoker invoker = null)
        {
            
        }
    }
}