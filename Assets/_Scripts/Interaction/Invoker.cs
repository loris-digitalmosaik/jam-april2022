using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Scripts.Interaction
{
    public abstract class Invoker : MonoBehaviour
    {
        protected bool WantInvoke = true;
        [ShowInInspector]
        protected readonly HashSet<Receiver> ActiveReceivers = new();

        protected abstract void OnTriggerEnter(Collider other);

        protected abstract void OnTriggerExit(Collider other);
    }
}