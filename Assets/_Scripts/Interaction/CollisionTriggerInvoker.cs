using Sirenix.Utilities;
using UnityEngine;

namespace _Scripts.Interaction
{
    [RequireComponent(typeof(Collider))]
    public class CollisionTriggerInvoker : Invoker
    {
        protected override void OnTriggerEnter(Collider other)
        {
            Receiver[] receivers = other.gameObject.GetComponents<Receiver>();
            if(receivers == null) return;
            ActiveReceivers.AddRange(receivers);
            if (WantInvoke)
            {
                foreach (Receiver receiver in receivers)
                {
                    receiver.ActionStart(this);
                }
            }
        }

        protected override void OnTriggerExit(Collider other)
        {
            Receiver[] receivers = other.gameObject.GetComponents<Receiver>();
            if(receivers == null) return;
            foreach (Receiver receiver in receivers)
            {
                ActiveReceivers.Remove(receiver);
                if (WantInvoke)
                {
                    receiver.ActionEnd(this);
                }
            }
        }
    }
}