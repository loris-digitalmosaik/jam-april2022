using UnityEngine;
namespace _Scripts.Interaction
{
    public class PlayerInvoker : CollisionTriggerInvoker
    {
        protected virtual void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out Receiver receiver))
            {
                receiver.ActionStart(this);
            }
        }
    }
}