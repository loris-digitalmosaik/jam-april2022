using UnityEngine.InputSystem;
namespace _Scripts.Interaction
{
    public class InputAndCollisionTriggerInvoker : CollisionTriggerInvoker
    {
        public InputActionReference input;

        protected virtual void Awake()
        {
            WantInvoke = false;
            input.action.Enable();
        }
        
        protected void OnEnable()
        {
            input.action.started += actionOnstarted;
            input.action.canceled += actionOncanceled;
        }
        
        protected void OnDisable()
        {
            WantInvoke = false;
            ActiveReceivers.Clear();
            input.action.performed -= actionOnstarted;
            input.action.canceled -= actionOncanceled;
        }

        protected virtual void actionOnstarted(InputAction.CallbackContext context)
        {
            WantInvoke = true;
            foreach (Receiver receiver in ActiveReceivers)
            {
                receiver.ActionStart(this);
            }
        }
        protected virtual void actionOncanceled(InputAction.CallbackContext context)
        {
            WantInvoke = false;
            foreach (Receiver receiver in ActiveReceivers)
            {
                receiver.ActionEnd();
            }
        }
    }
}