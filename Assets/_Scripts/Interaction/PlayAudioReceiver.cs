using System;
using _Scripts.Audio;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
namespace _Scripts.Interaction
{
    public class PlayAudioReceiver : Receiver
    {
        public AudioComponent audioComponent;
        public AudioClip clip;
        [Range(0f, 1f)]
        public float targetVolume = 1f;
        [BoxGroup("Blend Settings")]
        public bool blendClip = false;
        [ShowIf("@this.blendClip")]
        [BoxGroup("Blend Settings")]
        public float timeFadeOut = 1f;
        [ShowIf("@this.blendClip")]
        [BoxGroup("Blend Settings")]
        public float timeFadeIn = 1f;
        [ShowIf("@this.blendClip")]
        [BoxGroup("Blend Settings")]
        public float minVolume = 0f;

        private void Awake()
        {
            if (audioComponent == null)
            {
                audioComponent = gameObject.GetComponent<AudioComponent>();
            }
            if (audioComponent == null)
            {
                throw new NullReferenceException($"Aggiungere AudioComponent a {gameObject}");
            }
        }

        public override void ActionStart(Invoker invoker = null)
        {
            if (invoker is PlayerInvoker playerInvoker)
            {
                if (blendClip)
                {
                    throw new NotImplementedException();
                    // DOTween.To(() => audioComponent.audioSource.volume,
                    //     x => audioComponent.audioSource.volume = x,
                    //     minVolume, timeFadeOut).OnComplete(() => {
                    //     audioComponent.clip = clip;
                    //     audioComponent.Play();
                    //     DOTween.To(() => audioComponent.volume,
                    //         x => audioComponent.volume = x,
                    //         targetVolume, timeFadeIn);
                    // });
                }
                else
                {
                    audioComponent._SetVolume(targetVolume);
                    audioComponent._PlayClip(clip);
                }
                if (triggerOnlyOnce)
                {
                    Destroy(this);
                }
            }
        }

        public override void ActionEnd(Invoker invoker = null) { }
    }
}