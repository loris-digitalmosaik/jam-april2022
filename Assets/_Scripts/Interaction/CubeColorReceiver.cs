using UnityEngine;

namespace _Scripts.Interaction
{
    [RequireComponent(typeof(MeshRenderer))]
    public class CubeColorReceiver : Receiver
    {
        public Color hitColor = Color.yellow;
        private MeshRenderer _renderer;
        private Color _color;

        private void Awake()
        {
            _renderer = gameObject.GetComponent<MeshRenderer>();
        }

        public override void ActionStart(Invoker invoker = null)
        {
            if (invoker != null)
            {
                Debug.Log($"ActionStart from {invoker.gameObject.name} on {gameObject.name}");
            }
            _color = _renderer.material.color;
            _renderer.material.color = hitColor;
        }

        public override void ActionEnd(Invoker invoker = null)
        {
            Debug.Log("ActionEnd");
            _renderer.material.color = _color;
        }
    }
}