using Sirenix.OdinInspector;
using UnityEngine;

namespace _Scripts.Interaction
{
    [RequireComponent(typeof(Collider))]
    public abstract class Receiver : MonoBehaviour
    {
        public bool triggerOnlyOnce;

        public abstract void ActionStart(Invoker invoker = null);
        public abstract void ActionEnd(Invoker invoker = null);
    }
}