using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : SingletonB<GameManager>
{
    public InputActionReference inputMovement;

    public GameObject mover;
    public GameObject environment;
    public float minSpeed = 5f;
    public float maxSpeed = 50f;
    public Vector2 clampPosition = Vector2.one;
    public Animator playerAnimator;
    public GameObject panelEnd;
    public GameObject panelStart;
    public TMP_Text endLabel;
    
    private Vector2 _movement;
    private Vector2 _position;
    [HideInEditorMode]
    public float speed;
    private bool _isFall;
    private float _previousSide;

    private long _time;
    protected override void Awake()
    {
        base.Awake();
        inputMovement.action.Enable();
        _position = new Vector2(mover.transform.position.x, mover.transform.position.z);
        speed = minSpeed;
        _previousSide = 0f;
        _isFall = false;
        panelEnd.SetActive(false);
    }

    private void Start()
    {
        _time = DateTimeOffset.Now.ToUnixTimeSeconds();
        StartCoroutine(StartCO());
        StartCoroutine(UpdateAnimatorCO());
    }

    private void Update()
    {
        if (_isFall) return;
        _movement = inputMovement.action.ReadValue<Vector2>();
        
        mover.transform.position = Vector3.Lerp(
            mover.transform.position,
            new Vector3(_position.x + _movement.x * clampPosition.x, mover.transform.position.y, _position.y + _movement.y * clampPosition.y),
            Time.deltaTime);
        environment.transform.position -= Vector3.forward * speed * Time.deltaTime;
    }

    private IEnumerator StartCO()
    {
        panelStart.SetActive(true);
        yield return new WaitForSeconds(5f);
        panelStart.SetActive(false);
    }

    private IEnumerator UpdateAnimatorCO()
    {
        while (enabled)
        {
            float side = mover.transform.position.x - _position.x;
            playerAnimator.SetFloat("Direction", (side - _previousSide) / (clampPosition.x * 0.1f));
            _previousSide = mover.transform.position.x - _position.x;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ChangeSpeed(float value)
    {
        Debug.Log($"Change Speed {value}");
        speed = Mathf.Clamp(speed + value, minSpeed, maxSpeed);
    }

    public IEnumerator FallCO()
    {
        float value = speed;
        speed = 0f;
        _isFall = true;
        playerAnimator.SetTrigger("Fall");
        yield return new WaitForSeconds(2.5f);
        speed = value;
        _isFall = false;
    }

    public void Jump()
    {
        playerAnimator.SetTrigger("Jump");
        mover.transform.DOLocalJump(mover.transform.position, 1f, 1, 0.5f);
    }

    public IEnumerator EndGame()
    {
        enabled = false;
        playerAnimator.SetTrigger("Lose");
        playerAnimator.transform.DOLocalRotate(Vector3.down * 180f, 1f);
        mover.transform.DOLocalRotate(Vector3.up * 180f, 1f);
        yield return new WaitForSeconds(3f);
        _time = DateTimeOffset.Now.ToUnixTimeSeconds() - _time;
        endLabel.text = $"{endLabel.text} {_time}";
        panelEnd.SetActive(true);
    }
}
